# MacOS System Configuration

This repository contains information about my personal MacOS system configuration, including hardware and software specifications, customizations, andœ third-party tools.

## System Specifications

- Model: MacBook Pro (14-inch, 2023)
- Processor: Apple M2 Pro
- Memory: 16 GB
- Storage: 512 GB SSD
- Operating System: macOS Ventura

## Customizations

- Dock: Moved to the left side of the screen and set smaller size.
- Keep in dock:
  - Safari.
  - WhatsApp.
  - VSCode.
  - iTerm2.

## Third-Party Tools

- Clipy: a Clipboard extension app for macOS.
- iTerm2: a terminal emulator that provides more advanced features than the default Terminal app.
- OhMyZsh: a popular framework for managing your Zsh configuration, including themes, plugins, and customizations.
- VS Code: a code editor that I use for programming and text editing.
- Docker Desktop: a tool for creating and managing containerized applications, installed only for my user (no root password needed).
- Draw.io (desktop app): a web-based diagramming tool for creating flowcharts, network diagrams, and other visualizations.
- Git Credential Manager: a tool that securely stores your Git credentials and allows you to authenticate with Git servers without typing your username and password.
- AWS CLI: a command-line interface for interacting with Amazon Web Services, used for deploying and managing cloud-based applications and services.
- GitLab CLI: a command-line interface for interacting with GitLab, used for managing repositories, issues, and other GitLab features.
- Homebrew: the missing package manager for macOS.
- yq: a lightweight and portable command-line YAML, JSON and XML processor
- imgcat: it's like cat but for images.

## Other Information

- My favorite apps include Spotify, WhatsApp and Lightroom.
